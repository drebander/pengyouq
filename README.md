# 模仿微信朋友圈

#### 介绍
简单模仿朋友圈发布

#### 软件架构
下载宝塔，宝塔里下载mysql5.7版本，修改root密码为12345678，数据库node.js


#### 安装教程

1.  新建名为wx数据库
2.  后台运行node server.js
3.  数据库出现user和publish两个表

#### 使用说明

1.  运行代码
2.  打开发现页面
![输入图片说明](https://images.gitee.com/uploads/images/2021/0630/170309_00966903_8473904.jpeg "{5B447E1B-F806-419F-9B79-095E07A2075D}.png.jpg")
3.  可按微信添加朋友圈
![输入图片说明](https://images.gitee.com/uploads/images/2021/0630/170330_365c8971_8473904.jpeg "{463D2A13-03AF-47B2-9B83-A7261936930A}.png.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0630/170350_419be6e2_8473904.jpeg "{0EDC5163-AEE4-45AA-A340-87C2FE2D8DE7}.png.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0630/170402_59f0212f_8473904.jpeg "{A9AE0678-88CD-4253-932D-36F33806E8F1}.png.jpg")
#### 参与贡献

 黄洁琦

#### 特技


